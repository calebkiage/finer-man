package org.kiage.systems.finerman.common.constants

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/12/17
 */
object OauthConstants {
    object ClientId {
        val APP_CLIENT = "finer-man-app"
        val API_CLIENT = "finer-man-api"
    }
    object ResourceId {
        val WEB_API_SERVICE = "finer-man-web"
    }
}