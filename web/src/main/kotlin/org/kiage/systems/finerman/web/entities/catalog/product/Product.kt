package org.kiage.systems.finerman.web.entities.catalog.product

import org.hibernate.annotations.Cascade
import org.hibernate.annotations.Type
import org.kiage.systems.finerman.web.entities.catalog.category.Category
import org.kiage.systems.finerman.web.entities.catalog.image.ProductImage
import org.kiage.systems.finerman.web.entities.catalog.product.availability.ProductAvailability
import java.math.BigDecimal
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/28/17
 */
@Entity
class Product(
        var available: Boolean = true,

        @Column(nullable = false)
        var currency: String = "KES",
        var dateAvailable: ZonedDateTime = ZonedDateTime.now(),

        @Type(type = "org.hibernate.type.MaterializedClobType")
        var description: String? = null,

        @GeneratedValue
        @Id
        val id: Long = 0,

        @Column(nullable = false)
        var name: String = "",
        var price: BigDecimal = BigDecimal.ZERO,
        var priceSpecialAmount: BigDecimal? = null,
        var priceSpecialEndDate: ZonedDateTime? = null,
        var priceSpecialStartDate: ZonedDateTime? = null,
        var productIsFree: Boolean = false,
        var productShipable: Boolean = false
) {
    @OneToMany(cascade = arrayOf(CascadeType.REMOVE), mappedBy = "product", orphanRemoval = true)
    val availabilities: MutableSet<ProductAvailability> = HashSet()

    @ManyToMany(cascade = arrayOf(CascadeType.REFRESH))
    @JoinTable(
            name = "product_category",
            joinColumns = arrayOf(JoinColumn(name = "product_id", nullable = false, updatable = false)),
            inverseJoinColumns = arrayOf(JoinColumn(name = "category_id", nullable = false, updatable = false))
    )
    @Cascade(
            org.hibernate.annotations.CascadeType.DETACH,
            org.hibernate.annotations.CascadeType.LOCK,
            org.hibernate.annotations.CascadeType.REFRESH,
            org.hibernate.annotations.CascadeType.REPLICATE
    )
    val categories: MutableSet<Category> = HashSet()

    @OneToMany(cascade = arrayOf(CascadeType.REMOVE), mappedBy = "product")
    val images: MutableSet<ProductImage> = HashSet()

    fun getDefaultImage(): ProductImage? {
        return this.images.firstOrNull { it.defaultImage }
    }

    fun hasSpecial(): Boolean {
        val today = ZonedDateTime.now()

        // Return true if we are within the special period.
        return this.priceSpecialStartDate?.isBefore(today) == true && this.priceSpecialEndDate?.isAfter(today) == true
    }
}

