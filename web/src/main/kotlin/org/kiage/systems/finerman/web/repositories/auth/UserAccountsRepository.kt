package org.kiage.systems.finerman.web.repositories.auth

import org.kiage.systems.finerman.web.entities.auth.UserAccount
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface UserAccountsRepository : JpaRepository<UserAccount, Long> {
    @Transactional(readOnly = true)
    fun findByEmail(email: String): UserAccount?
}