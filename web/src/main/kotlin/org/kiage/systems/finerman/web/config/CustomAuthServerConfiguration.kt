package org.kiage.systems.finerman.web.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerSecurityConfiguration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource


/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            17/02/17
 */
//workaround for https://github.com/spring-projects/spring-security-oauth/issues/330
@Configuration
@Import(AuthorizationServerEndpointsConfiguration::class)
class CustomAuthServerConfiguration : AuthorizationServerSecurityConfiguration() {
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors().configurationSource(corsConfigurationSource())
        super.configure(http)
    }

    private fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration().applyPermitDefaultValues()
        source.registerCorsConfiguration("/oauth/**", config)
        return source
    }
}