package org.kiage.systems.finerman.web.entities.order

import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
@Entity
class UserOrder(
        var deliveryAddress: String = "",

        @CreationTimestamp
        var createdOn: ZonedDateTime = ZonedDateTime.now(),

        @GeneratedValue @Id
        val id: Long = 0,
        var phoneNumber: String = "",
        var userId: Long = 0
) {
    @OneToMany(cascade = arrayOf(CascadeType.ALL), mappedBy = "userOrder", orphanRemoval = true)
    val orderDetails: MutableSet<OrderDetail> = HashSet()
}