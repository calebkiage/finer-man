package org.kiage.systems.finerman.web.entities.auth

import java.util.*
import javax.persistence.*

@Entity
data class UserAccount(
        var accountNonExpired: Boolean = false,
        var accountNonLocked: Boolean = false,
        var credentialsNonExpired: Boolean = false,

        @Column(nullable = false, unique = true)
        var email: String,
        var emailVerification: String? = null,
        var enabled: Boolean = false,
        var firstName: String? = null,
        var gender: Gender = Gender.OTHER,
        var hashedPassword: String? = null,

        @GeneratedValue @Id
        val id: Long = 0,
        var lastName: String? = null
) {
    @OneToMany(cascade = arrayOf(CascadeType.ALL), mappedBy = "userAccount", orphanRemoval = true)
    val userAccountRoles: MutableSet<UserAccountRole> = HashSet(0)

    fun addRole(role: Role): Boolean {
        if (this.userAccountRoles.map { it.role }.any { it == role }) return false

        val userAccountRole = UserAccountRole(role = role, userAccount = this)
        return this.userAccountRoles.add(userAccountRole)
    }

    fun removeRole(role: Role): Boolean {
        if (!this.userAccountRoles.map { it.role }.any { it == role }) return false

        val userAccountRole = this.userAccountRoles.filter { it.role == role }.first()
        return this.userAccountRoles.remove(userAccountRole)
    }
}