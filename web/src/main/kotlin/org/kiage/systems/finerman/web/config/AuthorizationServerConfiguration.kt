package org.kiage.systems.finerman.web.config

import org.kiage.systems.finerman.common.constants.OauthConstants
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.approval.ApprovalStore
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore
import javax.sql.DataSource


/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/9/17
 */
@Configuration
@Order(-4)
class AuthorizationServerConfiguration(val dataSource: DataSource, val passwordEncoder : PasswordEncoder) : AuthorizationServerConfigurerAdapter() {
    @Bean
    fun tokenStore(): TokenStore {
        return JdbcTokenStore(this.dataSource)
    }

    @Bean
    protected fun authorizationCodeServices(): AuthorizationCodeServices {
        return JdbcAuthorizationCodeServices(this.dataSource)
    }

    @Bean
    fun approvalStore(): ApprovalStore {
        return JdbcApprovalStore(this.dataSource)
    }

    override fun configure(security: AuthorizationServerSecurityConfigurer?) {
        security!!.checkTokenAccess("isAuthenticated()").passwordEncoder(this.passwordEncoder)
    }

    override fun configure(clients: ClientDetailsServiceConfigurer?) {
        super.configure(clients)
        // @formatter:off
        clients!!
            .jdbc(this.dataSource)
                .withClient(OauthConstants.ClientId.API_CLIENT)
                .secret(this.passwordEncoder.encode("secret"))
                .scopes("read", "write", "trusted")
                .autoApprove(true)
                .authorizedGrantTypes("authorization_code", "refresh_token")
                .resourceIds(
                        OauthConstants.ResourceId.WEB_API_SERVICE
                )
            .and()
                .withClient(OauthConstants.ClientId.APP_CLIENT)
                .secret(this.passwordEncoder.encode("secret"))
                .scopes("read", "write", "trusted")
                .authorizedGrantTypes("implicit", "authorization_code", "refresh_token")
                .redirectUris("http://localhost:4200/account/login", "http://finer-man-client.s3-website.eu-west-2.amazonaws.com/account/login")
                .resourceIds(
                        OauthConstants.ResourceId.WEB_API_SERVICE
                )
        // @formatter:on
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer?) {
        endpoints!!.tokenStore(this.tokenStore())
                .authorizationCodeServices(this.authorizationCodeServices())
                .approvalStore(this.approvalStore())
    }
}