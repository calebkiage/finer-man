package org.kiage.systems.finerman.web.entities.auth

/**
 * Copyright © 2017
 * Created by Caleb on 1/23/2017.
 */
enum class Gender {
    OTHER,
    MALE,
    FEMALE
}