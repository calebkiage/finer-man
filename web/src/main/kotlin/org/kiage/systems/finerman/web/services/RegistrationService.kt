package org.kiage.systems.finerman.web.services

import org.kiage.systems.finerman.web.entities.auth.Role
import org.kiage.systems.finerman.web.entities.auth.UserAccount
import org.kiage.systems.finerman.web.repositories.auth.RolesRepository
import org.kiage.systems.finerman.web.repositories.auth.UserAccountsRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Copyright © 2017
 * Created by Caleb on 1/23/2017.
 */
@Service
class RegistrationService(
        val passwordEncoder: PasswordEncoder,
        val rolesRepository: RolesRepository,
        val userAccountsRepository: UserAccountsRepository) {
    /**
     * Finds a role with the specified name or creates one if no such role exists.
     *
     * @param roleName the name of the role.
     * @return The existing or newly created role.
     */
    @Transactional
    fun getOrCreateRole(roleName: String): Role {
        // Find the role or create it
        var role = this.rolesRepository.findOneByName(roleName)
        if (role == null) {
            role = Role(name = roleName)
            this.rolesRepository.save(role)
        }

        return role
    }

    /**
     * Registers a user with the password specified.
     *
     * @param userData the user's data. Don't fill in the hashed password.
     * @param password the user's desired password.
     * @param roles the roles to assign the user.
     * @return True if a user was created successfully. Otherwise, false.
     */
    @Transactional
    @Throws(IllegalArgumentException::class, Exception::class)
    fun registerUser(userData: UserAccount, password: String, roles: Array<String> = arrayOf("USER")): UserAccount {
        val existing = this.userAccountsRepository.findByEmail(userData.email.orEmpty())

        // If the user already exists, don't create another
        if (existing != null) {
            throw IllegalArgumentException("The username already exists.")
        }

        // Set flags.
        userData.accountNonExpired = true
        userData.accountNonLocked = true
        userData.credentialsNonExpired = true
        userData.enabled = true

        // Hash the password
        userData.hashedPassword = this.passwordEncoder.encode(password)

        // Add the user roles
        for (role in roles) {
            userData.addRole(this.getOrCreateRole(role))
        }

        val newUser = this.userAccountsRepository.save(userData)
        if (newUser != null) {
            return newUser
        } else {
            throw Exception("Failed to create user.")
        }
    }
}