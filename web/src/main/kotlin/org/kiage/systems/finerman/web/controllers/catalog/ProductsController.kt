package org.kiage.systems.finerman.web.controllers.catalog

import com.querydsl.core.types.Order
import com.querydsl.core.types.OrderSpecifier
import org.kiage.systems.finerman.web.entities.catalog.product.Product
import org.kiage.systems.finerman.web.entities.catalog.product.QProduct
import org.kiage.systems.finerman.web.models.catalog.ProductModel
import org.kiage.systems.finerman.web.repositories.catalog.product.ProductsRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.QPageRequest
import org.springframework.data.querydsl.QSort
import org.springframework.data.web.PageableDefault
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            18/02/17
 */
@RestController
@RequestMapping("products")
class ProductsController(val productsRepository : ProductsRepository) {
    @GetMapping("/latest")
    fun getLatest(): List<ProductModel> {
        val orderBy = OrderSpecifier(Order.DESC, QProduct.product.dateAvailable)
        val page = QPageRequest(0, 5, QSort(orderBy))
        return this.productsRepository.findAll(page).map { ProductModel(it) }.toList()
    }

    @GetMapping
    fun getProducts(
            @PageableDefault(size = 20, sort = arrayOf("name"))
            paging: Pageable
    ): Page<ProductModel> {
        return this.productsRepository.findAll(paging).map { ProductModel(it) }
    }

    @GetMapping("/{id}")
    fun getProduct(@PathVariable("id") product: Product): ProductModel {
        return ProductModel(product)
    }
}