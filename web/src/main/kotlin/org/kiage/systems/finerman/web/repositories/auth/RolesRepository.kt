package org.kiage.systems.finerman.web.repositories.auth

import org.kiage.systems.finerman.web.entities.auth.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RolesRepository : JpaRepository<Role, Long> {
    fun findOneByName(roleName: String): Role?
}