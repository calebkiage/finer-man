package org.kiage.systems.finerman.web.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.csrf.CsrfTokenRepository

/**
 * Copyright © 2017
 * Created by Caleb on 1/23/2017.
 */
@Configuration
@Order(-5)
class AuthWebSecurityConfiguration(
        val csrfTokenRepository: CsrfTokenRepository,
        val passwordEncoder: PasswordEncoder,
        val userDetailsService: UserDetailsService
) : WebSecurityConfigurerAdapter() {
    // This comes after the resource server config.
    override fun configure(http: HttpSecurity?) {
        // @formatter:off
        http!!
            .requestMatchers()
                .antMatchers("/oauth/authorize", "/login")
                .and()
            .authorizeRequests()
                .antMatchers("/oauth/authorize").fullyAuthenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .failureUrl("/login?authentication_error=true")
                .permitAll()
                .and()
            .logout()
                .permitAll()
                .and()
            .csrf()
                .ignoringAntMatchers("/oauth/token")
                .csrfTokenRepository(this.csrfTokenRepository)

        // @formatter:on
    }

    // Use this everywhere. Even actuators.
    @Autowired
    fun globalAuthenticationManager(builder: AuthenticationManagerBuilder): Unit {
        builder.userDetailsService(this.userDetailsService)
                .passwordEncoder(this.passwordEncoder)
    }
}