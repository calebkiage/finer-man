package org.kiage.systems.finerman.web.models.catalog

import org.kiage.systems.finerman.web.entities.catalog.product.Product
import java.math.BigDecimal
import java.time.ZonedDateTime

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            18/02/17
 */
class ProductModel() {
    constructor(product: Product) : this() {
        this.available = product.available
        this.currency = product.currency
        this.dateAvailable = product.dateAvailable
        this.defaultImage = product.getDefaultImage()?.productImageUrl
        this.description = product.description
        this.hasSpecial = product.hasSpecial()
        this.id = product.id
        this.images.addAll(product.images.filter { !it.productImageUrl.isNullOrBlank() }.map { it.productImageUrl!! }.toMutableList())
        this.name = product.name
        this.price = product.price
        this.priceSpecialAmount = product.priceSpecialAmount
        this.priceSpecialEndDate = product.priceSpecialEndDate
        this.priceSpecialStartDate = product.priceSpecialStartDate
        this.productIsFree = product.productIsFree
        this.productShipable = product.productShipable
    }

    var available: Boolean = true
    var currency: String = "KES"
    var dateAvailable: ZonedDateTime = ZonedDateTime.now()
    var defaultImage: String? = null
    var description: String? = null
    var hasSpecial: Boolean = false
    var id: Long = 0
    var images: MutableList<String> = mutableListOf()
    var name: String = ""
    var price: BigDecimal = BigDecimal.ZERO
    var priceSpecialAmount: BigDecimal? = null
    var priceSpecialEndDate: ZonedDateTime? = null
    var priceSpecialStartDate: ZonedDateTime? = null
    var productIsFree: Boolean = false
    var productShipable: Boolean = false
}