package org.kiage.systems.finerman.web.controllers.catalog

import com.querydsl.core.types.Predicate
import org.kiage.systems.finerman.web.entities.catalog.category.Category
import org.kiage.systems.finerman.web.repositories.catalog.category.CategoriesRepository
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/12/17
 */
@RestController
@RequestMapping("/categories")
class CategoriesController(val categoriesRepository : CategoriesRepository) {
    @GetMapping
    @PreAuthorize("#oauth2.hasScope('read')")
    fun getCategories(predicate: Predicate?): List<Category> {
        return this.categoriesRepository.findAll(predicate).toList()
    }
}