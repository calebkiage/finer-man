package org.kiage.systems.finerman.web.repositories.order

import org.javers.spring.annotation.JaversSpringDataAuditable
import org.kiage.systems.finerman.web.entities.order.UserOrder
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
@Repository
@JaversSpringDataAuditable
interface OrdersRepository : PagingAndSortingRepository<UserOrder, Long>, QuerydslPredicateExecutor<UserOrder>