package org.kiage.systems.finerman.web.entities.auth

@javax.persistence.Entity
data class UserAccountRole(
        @javax.persistence.Id
        @javax.persistence.ManyToOne(optional = false)
        @javax.persistence.JoinColumn(name = "role_id")
        var role: Role? = null,

        @javax.persistence.Id
        @javax.persistence.ManyToOne(optional = false)
        @javax.persistence.JoinColumn(name = "user_account_id")
        var userAccount: UserAccount? = null
) : java.io.Serializable