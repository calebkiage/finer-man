package org.kiage.systems.finerman.web.services

import org.kiage.systems.finerman.web.repositories.auth.UserAccountsRepository
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class AuthUserDetailsService(val userAccountsRepository: UserAccountsRepository) : UserDetailsService {
    @Transactional(readOnly = true)
    override fun loadUserByUsername(email: String?): UserDetails {
        if (email.isNullOrBlank()) throw IllegalArgumentException("Please specify a username or an email.")

        val userAccount = this.userAccountsRepository.findByEmail(email.orEmpty())
                ?: throw UsernameNotFoundException("User not found.")

        val userDetails = User(
                userAccount.email,
                userAccount.hashedPassword,
                userAccount.enabled,
                userAccount.accountNonExpired,
                userAccount.credentialsNonExpired,
                userAccount.accountNonLocked,
                userAccount.userAccountRoles.map { it.role }.map { SimpleGrantedAuthority("ROLE_${it?.name}") }
        )

        return userDetails
    }
}