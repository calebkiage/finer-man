package org.kiage.systems.finerman.web.entities.catalog.category

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/28/17
 */
@Entity
class Category(
        @Type(type = "org.hibernate.type.MaterializedClobType")
        var description: String? = null,

        @GeneratedValue
        @Id
        val id: Long = 0,

        @Column(nullable = false)
        var name: String = "",

        @ManyToOne
        var parent: Category? = null,

        var categoryImage: String? = null,
        var sortOrder: Int = 0,
        var categoryStatus: Boolean = true,
        var visible: Boolean = true,

        @Column(length = 100, nullable = false)
        var code: String
) {
    @OneToMany(mappedBy = "parent", cascade = arrayOf(CascadeType.REMOVE), orphanRemoval = true)
    val categories: MutableList<Category> = ArrayList()
}