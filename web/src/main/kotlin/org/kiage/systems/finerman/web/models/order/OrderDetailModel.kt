package org.kiage.systems.finerman.web.models.order

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
class OrderDetailModel {
    var id: Long = 0
    var quantity: Int = 0
    var product: Map<String, Any> = emptyMap()
}