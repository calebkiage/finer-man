package org.kiage.systems.finerman.web.repositories.catalog.category

import org.javers.spring.annotation.JaversSpringDataAuditable
import org.kiage.systems.finerman.web.entities.catalog.category.Category
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/29/17
 */
@Repository
@JaversSpringDataAuditable
interface CategoriesRepository : PagingAndSortingRepository<Category, Long>, QuerydslPredicateExecutor<Category>