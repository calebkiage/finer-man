package org.kiage.systems.finerman.web.config

import org.kiage.systems.finerman.web.repositories.auth.UserAccountsRepository
import org.kiage.systems.finerman.web.services.AuthUserDetailsService
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.csrf.CookieCsrfTokenRepository
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@EnableJpaRepositories("org.kiage.systems.finerman.web.repositories")
@EnableTransactionManagement
@EntityScan("org.kiage.systems.finerman.web.entities")
class FinerManAuthBeans {
    @Bean
    fun corsConfigurer(): WebMvcConfigurer {
        return object: WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry?) {
                registry!!.addMapping("/**")
            }
        }
    }

    @Bean
    fun csrfTokenRepository(): CsrfTokenRepository {
        return CookieCsrfTokenRepository.withHttpOnlyFalse()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun userDetailsService(userAccountsRepository: UserAccountsRepository): AuthUserDetailsService {
        return AuthUserDetailsService(userAccountsRepository)
    }

    @Bean
    fun viewsConfigurer(): WebMvcConfigurer {
        return object: WebMvcConfigurer {
            @Throws(Exception::class)
            override fun addViewControllers(registry: ViewControllerRegistry) {
                registry.addViewController("/login").setViewName("login")
                registry.addViewController("/oauth/confirm_access").setViewName("authorize")
            }
        }
    }
}