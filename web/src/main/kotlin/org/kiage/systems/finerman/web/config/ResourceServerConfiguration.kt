package org.kiage.systems.finerman.web.config

import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/11/17
 */
@Configuration
@EnableResourceServer
@Order(-3)
class ResourceServerConfiguration(
        val properties: ResourceServerProperties
) : ResourceServerConfigurerAdapter() {
    override fun configure(resources: ResourceServerSecurityConfigurer?) {
        resources!!.resourceId(properties.resourceId)
    }

    override fun configure(http: HttpSecurity?) {
        // formatter:off
        http!!
            .cors()
                .and()
            .authorizeRequests()
                .mvcMatchers("/user")
                .access("#oauth2.hasScope('read') and hasRole('USER')")
                .mvcMatchers("/accounts/register").permitAll()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/products/**").permitAll()
                .anyRequest().fullyAuthenticated()
        // formatter:on
    }
}
