package org.kiage.systems.finerman.web

import org.kiage.systems.finerman.web.entities.auth.UserAccount
import org.kiage.systems.finerman.web.entities.catalog.category.Category
import org.kiage.systems.finerman.web.entities.catalog.image.ProductImage
import org.kiage.systems.finerman.web.entities.catalog.product.Product
import org.kiage.systems.finerman.web.repositories.auth.UserAccountsRepository
import org.kiage.systems.finerman.web.repositories.catalog.category.CategoriesRepository
import org.kiage.systems.finerman.web.repositories.catalog.image.ProductImagesRepository
import org.kiage.systems.finerman.web.repositories.catalog.product.ProductsRepository
import org.kiage.systems.finerman.web.services.RegistrationService
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationListener
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import java.time.ZonedDateTime

@Component
class InitialDataLoader(
        @Value("\${fm.seed-data}")
        val seed: String?,
        val categoriesRepository: CategoriesRepository,
        val productImagesRepository: ProductImagesRepository,
        val productsRepository: ProductsRepository,
        val registrationService: RegistrationService,
        val userAccountsRepository: UserAccountsRepository
) : ApplicationListener<ContextRefreshedEvent> {
    @Transactional
    override fun onApplicationEvent(event: ContextRefreshedEvent?) {
        if (this.seed == null || this.seed == "false") {
            return
        }

        var adminAccount = this.userAccountsRepository.findByEmail("admin@mail.com")
        if (adminAccount == null) {
            adminAccount = UserAccount(
                    email = "admin@mail.com"
            )
        }

        var userAccount = this.userAccountsRepository.findByEmail("user@mail.com")
        if (userAccount == null) {
            userAccount = UserAccount(
                    email = "user@mail.com"
            )
        }

        this.registrationService.registerUser(adminAccount, "password", arrayOf("ACTUATOR", "ADMIN", "USER"))
        this.registrationService.registerUser(userAccount, "password")

        // Authentication to use for seeding.
        val auth = UsernamePasswordAuthenticationToken("system-seed", "")
        SecurityContextHolder.getContext().authentication = auth

        // Categories
        val catFootwear = Category(
                name = "Footwear",
                code = "footwear"
        )
        val catWinterWear = Category(
                code = "winter-wear",
                name = "Winter Wear"
        )
        val catApparel = Category(
                code = "apparel",
                name = "Apparel"
        )

        // Save Categories
        this.categoriesRepository.saveAll(arrayListOf(catApparel, catFootwear, catWinterWear))

        // Products.
        val product1 = Product(
                name = "Leather Boots",
                description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum enim magni modi nesciunt vero. Asperiores ea error fuga fugiat obcaecati quam vero vitae? Est excepturi non recusandae suscipit tenetur voluptas?",
                price = BigDecimal(2000),
                priceSpecialAmount = BigDecimal(1850),
                priceSpecialEndDate = ZonedDateTime.now().plusDays(2),
                priceSpecialStartDate = ZonedDateTime.now().minusDays(1)
        )
        val product2 = Product(
                name = "Blazer",
                description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt ea explicabo quaerat quia ratione reiciendis repellat saepe tempore velit. Asperiores blanditiis delectus excepturi harum molestiae perspiciatis rem suscipit tempora voluptatibus.",
                price = BigDecimal(1090)
        )
        val product3 = Product(
                name = "Levi's Frames",
                description = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum harum illum, necessitatibus nisi quibusdam quis recusandae rem suscipit. Ab, accusamus autem culpa dolor et eum harum laudantium sed ut vel.",
                price = BigDecimal(2000),
                priceSpecialAmount = BigDecimal(1850),
                priceSpecialEndDate = ZonedDateTime.now().minusDays(1),
                priceSpecialStartDate = ZonedDateTime.now().minusDays(3)
        )

        // Add the categories
        product1.categories.add(catFootwear)
        product2.categories.add(catWinterWear)
        product3.categories.add(catApparel)

        // Save the products...
        this.productsRepository.saveAll(arrayListOf(product1, product2, product3))

        // then save the images. We don't have persist cascading.
        val image1 = ProductImage(productImageUrl = "/assets/images/1.jpg", product = product1)
        val image2 = ProductImage(productImageUrl = "/assets/images/2.jpg", product = product2)
        val image3 = ProductImage(productImageUrl = "/assets/images/5.jpg", product = product3)
        this.productImagesRepository.saveAll(arrayListOf(image1, image2, image3))

        SecurityContextHolder.clearContext()
    }
}