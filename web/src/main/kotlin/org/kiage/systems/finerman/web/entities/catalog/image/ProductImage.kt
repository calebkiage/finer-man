package org.kiage.systems.finerman.web.entities.catalog.image

import org.kiage.systems.finerman.web.entities.catalog.product.Product
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/28/17
 */
@Entity
class ProductImage(
        @GeneratedValue
        @Id
        val id: Long = 0,

        var productImage: String? = null,
        var defaultImage: Boolean = true,
        var imageType: Int = 0,
        var productImageUrl: String? = null,
        var imageCrop: Boolean = false,

        @ManyToOne
        @JoinColumn(nullable = false)
        var product: Product
)