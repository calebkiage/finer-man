package org.kiage.systems.finerman.web.entities.auth

@javax.persistence.Entity
data class Role(
        @javax.persistence.GeneratedValue @javax.persistence.Id
        val id: Long = 0,

        @javax.persistence.Column(nullable = false, unique = true)
        var name:String? = null
) {
    @javax.persistence.OneToMany(cascade = arrayOf(javax.persistence.CascadeType.ALL), mappedBy = "role", orphanRemoval = true)
    val userAccountRoles: MutableSet<UserAccountRole> = java.util.HashSet(0)

    fun addUserAccount(account: UserAccount): Boolean {
        if (this.userAccountRoles.any { it.userAccount == account }) return false

        val accountRole = UserAccountRole(role = this, userAccount = account);
        return this.userAccountRoles.add(accountRole);
    }

    fun removeUserAccount(account: UserAccount): Boolean {
        if (!this.userAccountRoles.any { it.userAccount == account }) return false

        val accountRole = this.userAccountRoles.filter { it.userAccount == account }.first()
        return this.userAccountRoles.remove(accountRole)
    }
}