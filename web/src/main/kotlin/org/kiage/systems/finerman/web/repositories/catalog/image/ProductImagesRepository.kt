package org.kiage.systems.finerman.web.repositories.catalog.image

import org.javers.spring.annotation.JaversSpringDataAuditable
import org.kiage.systems.finerman.web.entities.catalog.image.ProductImage
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.stereotype.Repository

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/29/17
 */
@Repository
@JaversSpringDataAuditable
interface ProductImagesRepository : JpaRepository<ProductImage, Long>, QuerydslPredicateExecutor<ProductImage>