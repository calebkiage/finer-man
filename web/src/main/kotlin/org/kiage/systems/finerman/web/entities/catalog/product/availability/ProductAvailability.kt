package org.kiage.systems.finerman.web.entities.catalog.product.availability

import org.kiage.systems.finerman.web.entities.catalog.product.Product
import java.time.ZonedDateTime
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            1/28/17
 */
@Entity
class ProductAvailability(
        @GeneratedValue
        @Id
        val id: Long = 0,

        @ManyToOne(optional = false)
        @JoinColumn(nullable = false)
        var product: Product,
        var productQuantity: Int = 0,
        var productDateAvailable: ZonedDateTime = ZonedDateTime.now(),
        var productStatus: Boolean = true,
        var productIsAlwaysFreeShipping: Boolean = false,
        var productQuantityOrderMin: Int = 0,
        var productQuantityOrderMax: Int = 0
) {
}