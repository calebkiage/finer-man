package org.kiage.systems.finerman.web

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class WebApiApplication {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(WebApiApplication::class.java, *args)
        }
    }
}
