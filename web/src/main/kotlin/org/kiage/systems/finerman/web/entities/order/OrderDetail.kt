package org.kiage.systems.finerman.web.entities.order

import java.math.BigDecimal
import javax.persistence.*

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
@Entity
class OrderDetail(
        @GeneratedValue @Id
        val id: Long = 0,

        @ManyToOne(optional = false)
        @JoinColumn(nullable = false)
        var userOrder: UserOrder,
        var productId: Long,
        var quantity: Int = 1,
        var price: BigDecimal = BigDecimal.ZERO
)