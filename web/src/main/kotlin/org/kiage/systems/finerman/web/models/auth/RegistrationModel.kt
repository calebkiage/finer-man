package org.kiage.systems.finerman.web.models.auth

import org.hibernate.validator.constraints.Email
import org.hibernate.validator.constraints.NotBlank
import org.kiage.systems.finerman.web.entities.auth.Gender
import javax.validation.constraints.Size

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            17/02/17
 */
class RegistrationModel {
    @NotBlank
    @Email
    var email: String = ""
    var firstName: String? = null
    var gender: Gender = Gender.OTHER
    var id: Long = 0
    var lastName: String? = null
    @NotBlank
    @Size(min = 6)
    var password: String = ""
    var passwordConfirmation: String? = null
}