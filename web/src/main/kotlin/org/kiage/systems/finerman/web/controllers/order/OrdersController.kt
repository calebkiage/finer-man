package org.kiage.systems.finerman.web.controllers.order

import org.kiage.systems.finerman.web.entities.order.OrderDetail
import org.kiage.systems.finerman.web.entities.order.QUserOrder
import org.kiage.systems.finerman.web.entities.order.UserOrder
import org.kiage.systems.finerman.web.models.order.OrderDetailModel
import org.kiage.systems.finerman.web.models.order.OrderModel
import org.kiage.systems.finerman.web.repositories.auth.UserAccountsRepository
import org.kiage.systems.finerman.web.repositories.catalog.product.ProductsRepository
import org.kiage.systems.finerman.web.repositories.order.OrdersRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.web.PageableDefault
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal
import java.security.Principal

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
@RestController
@RequestMapping("orders")
class OrdersController(val ordersRepository: OrdersRepository, val productsRepository: ProductsRepository, val userAccountsRepository: UserAccountsRepository) {
    @PostMapping("checkout")
    fun createOrder(@Validated @RequestBody model: OrderModel, principal: Principal): Long {
        // Don't allow null users.
        val userId = this.userAccountsRepository.findByEmail(principal.name)!!.id

        val order = UserOrder(deliveryAddress = model.deliveryAddress, phoneNumber = model.phoneNumber, userId = userId)
        order.orderDetails.addAll(model.orderDetails.map {
            val productId: Long = (it.product.get("id") as Int).toLong()
            OrderDetail(userOrder = order,
                    quantity = it.quantity,
                    productId = productId,
                    price = this.productsRepository.findById(productId).map { it.price }.orElse(BigDecimal.ZERO))
        })

        // Create order.
        return this.ordersRepository.save(order).id
    }

    @GetMapping
    fun getOrders(@PageableDefault(size = 20, sort = arrayOf("createdOn"), direction = Sort.Direction.DESC)
                  paging: Pageable, principal: Principal): Page<OrderModel> {
        val userId = this.userAccountsRepository.findByEmail(principal.name)!!.id
        return this.ordersRepository.findAll(QUserOrder.userOrder.userId.eq(userId), paging).map {
            val orderModel = OrderModel()
            orderModel.createdOn = it.createdOn
            orderModel.deliveryAddress = it.deliveryAddress
            orderModel.id = it.id
            orderModel.orderDetails = it.orderDetails.map {
                val detail = OrderDetailModel()
                val product = this.productsRepository.findById(it.productId).orElse(null)
                detail.id = it.id
                detail.product = mapOf(Pair("id", product.id), Pair("name", product.name))
                detail.quantity = it.quantity
                detail
            }
            orderModel.phoneNumber = it.phoneNumber

            orderModel
        }
    }
}