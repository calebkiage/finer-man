package org.kiage.systems.finerman.web.models.order

import java.time.ZonedDateTime

/**
 * Author:          Caleb Kiage
 * Email:           caleb.kiage@gmail.com
 * Date:            6/17/17
 */
class OrderModel {
    var id: Long = 0
    var createdOn: ZonedDateTime? = null
    var deliveryAddress: String = ""
    var phoneNumber: String = ""
    var orderDetails: List<OrderDetailModel> = emptyList()
}