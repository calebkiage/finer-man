package org.kiage.systems.finerman.web.controllers.auth

import org.kiage.systems.finerman.web.entities.auth.UserAccount
import org.kiage.systems.finerman.web.models.auth.RegistrationModel
import org.kiage.systems.finerman.web.services.RegistrationService
import org.springframework.context.MessageSource
import org.springframework.validation.BindingResult
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.security.Principal
import java.util.*


/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/10/17
 */
@RestController
class AccountsController(val messageSource: MessageSource, val registrationService : RegistrationService) {
    @GetMapping(path = arrayOf("user"))
    fun user(principal : Principal): Principal {
        return principal
    }
    
    @PostMapping("/accounts/register")
    fun registerAccount(@Validated @RequestBody model: RegistrationModel, bindingResult : BindingResult, locale : Locale): Any {
        val globalErrors = mutableListOf<String>()
        val fieldErrors = mutableMapOf<String, String>()
        val errors = mutableMapOf(Pair("global", globalErrors), Pair("field", fieldErrors))
        val errorResult = mapOf(Pair("errors", errors))
        // Handle binding errors.
        if (bindingResult.hasErrors()) {
            // Start by adding object errors.
            globalErrors.addAll(bindingResult.globalErrors.map {this.messageSource.getMessage(it.codes.first { this.messageSource.getMessage(it, null, locale) != it}, it.arguments, locale)})
            // Then add field errors.
            fieldErrors.putAll(bindingResult.fieldErrors.associateBy({ it.field }, {this.messageSource.getMessage(it.codes.first(), it.arguments, locale)}))

            // Check for password match.
            if (model.password != model.passwordConfirmation) {
                fieldErrors.put("passwordConfirmation", "The passwords don't match.")
            }
            return errorResult
        }

        val newUser = this.registrationService.registerUser(UserAccount(
                email = model.email,
                firstName = model.firstName,
                gender = model.gender,
                lastName = model.lastName
        ), model.password)

        model.id = newUser.id
        model.password = "[PROTECTED]"
        model.passwordConfirmation = "[PROTECTED]"

        return model
    }
}