package org.kiage.systems.finerman.web

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors
import org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            2/13/17
 */
@AutoConfigureMockMvc
@RunWith(SpringRunner::class)
@SpringBootTest
class WebApiApplicationTests {
	@Autowired
	lateinit var mvc: MockMvc

	@Test
	fun returnsLoginPageOnGet(): Unit {
		mvc.perform(MockMvcRequestBuilders
				.get("/login")
				.with(SecurityMockMvcRequestPostProcessors.anonymous()))

				.andExpect(MockMvcResultMatchers.status().isOk)
				.andExpect(MockMvcResultMatchers.view().name("login"))
	}

	@Test
	fun authenticatesOnLoginPost(): Unit {
		val user = "admin@mail.com"
		mvc.perform(SecurityMockMvcRequestBuilders.formLogin()
				.user(user)
				.password("password"))
				.andExpect(SecurityMockMvcResultMatchers
						.authenticated()
						.withUsername(user))
	}

	@Test
	fun redirectsToLoginPageOnUnauthorizedRequest(): Unit {
		mvc.perform(MockMvcRequestBuilders.get("/oauth/authorize"))
				.andExpect(MockMvcResultMatchers.status().isFound)
				.andExpect(MockMvcResultMatchers.redirectedUrl("http://localhost/login"))
	}

	@Test
	fun returns401OnUnauthorizedAccessToUserEndpoint(): Unit {
		mvc.perform(MockMvcRequestBuilders.get("/user"))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized)
	}

	@Test
	fun returns200OnPreFlightRequestToTokenEndpoint(): Unit {
		val origin = "finer-man-test.com"
		mvc.perform(MockMvcRequestBuilders.options("/oauth/token")
				.header(HttpHeaders.ORIGIN, origin)
				.header(HttpHeaders.ACCESS_CONTROL_REQUEST_METHOD, "POST"))
				.andExpect(MockMvcResultMatchers.status().isOk)
				.andExpect(MockMvcResultMatchers.header()
						.string(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "GET,HEAD,POST"))
				.andExpect(MockMvcResultMatchers.header()
						.string(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, origin))
	}

	@Test
	fun returns401OnUnauthorizedAccessToTokenEndpoint(): Unit {
		mvc.perform(MockMvcRequestBuilders.post("/oauth/token"))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized)
	}
}