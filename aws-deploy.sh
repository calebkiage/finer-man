#!/usr/bin/env sh

cd client
npm run deploy

cd ../

./gradlew web:bootjar
cd web
eb deploy
