import { Directive, OnInit } from '@angular/core';

declare let $: any;
declare let ScrollMagic: any;
declare let TimelineLite: any;
declare let TweenLite: any;

@Directive({
  selector: '[fmScrollAnimation]'
})
export class ScrollAnimationDirective implements OnInit {
  ngOnInit() {
    // init controller
    const controller = new ScrollMagic.Controller({container: '.fm-content > .pusher > .fm-body-wrapper', loglevel: 2});

    const bg = $('.fm-masthead-image');
    const icon = $('.fm-masthead-scroll-icon');
    const text = $('.fm-masthead-scroll-indicator > div:not(.fm-masthead-scroll-icon)');
    const scene_opts = {
      duration: '80%',
      offset: 1
    };

    const scroll_tl = new TimelineLite();
    // The background moves up.
    const tween1 = TweenLite.to(bg, 1, {css:{y: '-=250px'}});

    // The scroll icon and text move up at different rates.
    const tween2 = TweenLite.to(icon, .3, {css: {opacity: 0, y: '-=110px'}, delay: .01});
    const tween3 = TweenLite.to(text, .3, {css: {opacity: 0, y: '-=70px'}, delay: .01});

    // Add all the tweens to run at the same time
    scroll_tl.add([tween1, tween2, tween3]);

    const scene = new ScrollMagic.Scene(scene_opts)
      .setTween(scroll_tl)
      // .addIndicators()
      .addTo(controller);

    scene.on('enter', function () {
      icon.removeClass('fm-animated');
    });

    scene.on('leave', function () {
      icon.addClass('fm-animated');
    });
  }
}
