import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ProductsService } from '../shared/services/products.service';

@Component({
  selector: 'fm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  latestProducts: Array<any>;

  constructor(private productsService: ProductsService, title: Title) {
    title.setTitle('Finer Man - Welcome');
  }

  addToCart(product) {
    // TODO: Implement.
  }

  ngOnInit() {
    this.productsService.findLatest().subscribe(
      (products) => {
        this.latestProducts = products;
      },
      error => {
        // TODO: Handle error.
      });
  }
}
