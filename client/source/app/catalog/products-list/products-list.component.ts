import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/services/products.service';

@Component({
  selector: 'fm-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  products: any;

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.productsService.findAll().subscribe(products => this.products = products.content);
  }
}
