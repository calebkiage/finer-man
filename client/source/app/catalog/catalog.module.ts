import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CatalogRoutingModule } from './catalog-routing.module';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SharedModule
  ],
  declarations: [ProductsListComponent, ProductDetailsComponent]
})
export class CatalogModule { }
