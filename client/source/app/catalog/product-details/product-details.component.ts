import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../shared/services/products.service';

@Component({
  selector: 'fm-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  product: any;

  constructor(private activated: ActivatedRoute, private productsService: ProductsService) {
  }

  ngOnInit() {
    this.activated.params.filter(p => !!p).subscribe(p => {
      const productId = +p['productId'];

      this.productsService.findOne(productId).subscribe(product => this.product = product);
    });
  }
}
