import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { CART_REMOVE, CartState } from '../../common/states/shopping-cart.state';

@Component({
  selector: 'fm-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  cartCurrency: Observable<string>;
  cartItems: Observable<Array<any>>;
  cartTotal: Observable<number>;

  constructor(private store: Store<CartState>, private router: Router) {
  }

  checkout() {
    this.router.navigate(['orders', 'checkout'], {relativeTo: null})
  }

  removeItem(product) {
    this.store.dispatch({type: CART_REMOVE, payload: product})
  }

  ngOnInit() {
    this.cartItems = this.store.select<any>('cart').map((items: any[])=> {
      return _.map(items, (item)=> {
        item.total = item.quantity * (item.product.hasSpecial ? item.product.priceSpecialAmount: item.product.price);
        return item;
      })
    });

    this.cartTotal = this.cartItems.map(items=> {
      return _.reduce(_.map(items, item=> {
        return item.total;
      }), (sum, total)=> {
        return sum + total;
      }) || 0;
    });

    this.cartCurrency = this.cartItems.map(items=> {
      const group = _.groupBy(items, 'product.currency');
      const currencies = _.keys(group);
      return currencies && currencies.length === 1 ? currencies[0] : null;
    });
  }
}
