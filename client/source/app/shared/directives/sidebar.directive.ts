import { Directive, ElementRef, Input, Output, EventEmitter, OnInit } from '@angular/core';

declare let $: any;

@Directive({
  selector: '[fmSidebar]'
})
export class SidebarDirective implements OnInit {
  @Input()
  hideElementSelector: any;

  @Input()
  showElementSelector: any;

  @Input()
  sidebarContext: string;

  @Input()
  sidebarOptions: any;

  @Input()
  sidebarTransition: any;

  @Input()
  triggerElementSelector: any;

  @Output()
  sidebarOpened = new EventEmitter();

  @Output()
  sidebarClosed = new EventEmitter();

  constructor(private el: ElementRef) {
  }

  ngOnInit() {
    const context = $(this.sidebarContext);
    const options = $.extend({}, {
      context: context,
      onVisible: () => this.sidebarOpened.emit(),
      onHide: () => this.sidebarClosed.emit()
    }, this.sidebarOptions);

    $(this.el.nativeElement)
      .sidebar(options);

    if (this.hideElementSelector) {
      $(this.el.nativeElement).sidebar('attach events', this.hideElementSelector, 'hide');
    }

    if (this.showElementSelector) {
      $(this.el.nativeElement).sidebar('attach events', this.showElementSelector, 'show');
    }

    if (this.sidebarTransition) {
      $(this.el.nativeElement)
        .sidebar('setting', 'transition', this.sidebarTransition);
    }

    if (this.triggerElementSelector) {
      $(this.el.nativeElement)
        .sidebar('attach events', this.triggerElementSelector);
    }
  }
}
