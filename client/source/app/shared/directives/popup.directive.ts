import { Directive, ElementRef, OnInit, Input } from '@angular/core';

declare let $: any;

@Directive({
  selector: '[fmPopup]'
})
export class PopupDirective implements OnInit {
  @Input('fmPopupOptions')
  popupOptions: any;

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    $(this.el.nativeElement).popup(this.popupOptions);
  }
}
