import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ConsoleLogger } from '../common/console-logger';
import { Logger } from '../common/logger';
import { PopupDirective } from './directives/popup.directive';
import { SidebarDirective } from './directives/sidebar.directive';
import { FooterComponent } from './footer/footer.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { AuthorizationInterceptor } from './interceptors/authorization.interceptor';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { MenuSidebarComponent } from './menu-sidebar/menu-sidebar.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductsService } from './services/products.service';
import { TokenStoreService } from './services/token-store.service';
import { TokenService } from './services/token.service';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    FooterComponent,
    MenuBarComponent,
    MenuSidebarComponent,
    ProductCardComponent,
    ShoppingCartComponent,
    SidebarDirective,
    PopupDirective
  ],
  exports: [
    FooterComponent,
    MenuBarComponent,
    MenuSidebarComponent,
    ProductCardComponent,
    SidebarDirective,
    ShoppingCartComponent
  ],
  providers: [
    AuthGuardService,
    { multi: true, provide: HTTP_INTERCEPTORS, useClass: AuthorizationInterceptor },
    { provide: Logger, useClass: ConsoleLogger },
    ProductsService,
    TokenService,
    TokenStoreService
  ]
})
export class SharedModule {
}
