import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { AuthorizationToken } from './token-store.service';

@Injectable()
export class TokenService {
  constructor(private http: HttpClient) {
  }

  getTokenFromCode(code) {
    const headers = new HttpHeaders()
      .set('Authorization', `Basic ${btoa(`${environment.oauth.clientId}:${environment.oauth.clientSecret}`)}`)
      .append('Content-Type', 'application/x-www-form-urlencoded');
    const payload = `grant_type=authorization_code&code=${code}&redirect_uri=${environment.oauth.redirectUri}`;

    return this.http.post(environment.oauth.tokenEndpointUri, payload, {headers})
      .map(
        token => {
          return {
            loggedIn: true,
            token: token
          };
        }
      )
      .catch(error => this.catchError(error));
  }

  refreshAccessToken(oldToken: AuthorizationToken) {
    // First check the token.
    return this.http.get(environment.oauth.userEndpointUri, {
      headers: new HttpHeaders().set('Authorization', `${oldToken.token_type} ${oldToken.access_token}`)
    }).catch(error => this.catchError(error))
      .mergeMap(user => {
        const headers = new HttpHeaders()
          .set('Authorization', `Basic ${btoa(`${environment.oauth.clientId}:${environment.oauth.clientSecret}`)}`)
          .append('Content-Type', 'application/x-www-form-urlencoded');
        const payload = `grant_type=refresh_token&refresh_token=${oldToken.refresh_token}`;

        return this.http.post(environment.oauth.tokenEndpointUri, payload, {headers})
          .map(
            token => {
              return {
                loggedIn: true,
                token: token
              };
            }
          )
          .catch(error => this.catchError(error));
      });
  }

  private catchError(error) {
    const err = error.json();
    let userError = 'Could not fetch the access token.';
    switch (err.error) {
      case 'invalid_token':
      case 'invalid_grant':
        // Redo the authorization.
        userError = `${userError} Please try again.`;
        break;
      default:
        userError = err.error_description ? `${userError} ${err.error_description}` : userError;
        break;
    }
    return Observable.throw({
      loggedIn: false,
      error: userError
    });
  }
}
