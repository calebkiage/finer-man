import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class OrdersService {
  constructor(private http: HttpClient) {
  }

  createOrder(order) {
    return this.http.post(`${environment.backendUrl}/orders/checkout`, order);
  }

  getOrders() {
    return this.http.get<{content?}>(`${environment.backendUrl}/orders`);
  }
}
