/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TokenStoreService } from './token-store.service';

describe('TokenStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenStoreService]
    });
  });

  it('should show expired on no token', inject([TokenStoreService], (service: TokenStoreService) => {
    expect(service.isExpired()).toBe(true);
  }));
});
