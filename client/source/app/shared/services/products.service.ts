import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient) {
  }

  findAll() {
    return this.http.get<any>(`${environment.backendUrl}/products`);
  }

  findLatest() {
    return this.http.get<any>(`${environment.backendUrl}/products/latest`);
  }

  findOne(productId) {
    return this.http.get<any>(`${environment.backendUrl}/products/${productId}`);
  }
}
