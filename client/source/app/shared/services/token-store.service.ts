import { Injectable } from '@angular/core';
import { Logger } from '../../common/logger';
import * as moment from 'moment'

export interface AuthorizationToken {
  access_token: string;
  expires_in: number;
  issued_on: string;
  refresh_token: string;
  scope: string;
  token_type: string;
}

@Injectable()
export class TokenStoreService {
  private key = 'auth-token';
  private token: AuthorizationToken;

  constructor(private logger: Logger) { }

  clearToken() {
    this.token = null;
    localStorage.removeItem(this.key);
  }

  getToken(): AuthorizationToken {
    if (!this.token){
      this.token = JSON.parse(localStorage.getItem(this.key));
    }

    return this.token;
  }

  hasRefreshToken(): boolean {
    return this.getToken() && this.getToken().refresh_token && this.getToken().refresh_token.length > 0
  }

  isExpired(): boolean {
    return !this.getToken() || moment(this.getToken().issued_on).add(this.getToken().expires_in, 'seconds').isBefore(moment())
  }

  storeToken(tokenInfo: AuthorizationToken) {
    if (!tokenInfo || !tokenInfo.access_token) {
      this.logger.info('The authorization token is invalid.')
    } else {
      tokenInfo.issued_on = moment().toISOString();
      this.token = tokenInfo;
      localStorage.setItem(this.key, JSON.stringify(tokenInfo));
    }
  }
}
