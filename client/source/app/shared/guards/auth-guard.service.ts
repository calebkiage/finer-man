import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TokenStoreService } from '../services/token-store.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private tokenStore: TokenStoreService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.tokenStore.getToken() && !this.tokenStore.isExpired()) {
      return true;
    }

    this.router.navigate(['/account/login']);
    return false;
  }
}
