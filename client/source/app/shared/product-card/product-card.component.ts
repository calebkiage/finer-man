import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { CART_ADD } from '../../common/states/shopping-cart.state';

@Component({
  selector: 'fm-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input()
  product: any;

  constructor(private store: Store<any>) { }

  addToCart(product) {
    this.store.dispatch({type: CART_ADD, payload: product});
  }

  ngOnInit() {
  }
}
