import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'fm-menu-bar',
  templateUrl: 'menu-bar.component.html',
  styleUrls: ['menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {
  cartItemsCount: number;
  cartTotal;
  cartCurrency;

  @Input()
  isMenuOpen: boolean;

  constructor(private store: Store<any>) {
  }

  ngOnInit() {
    this.store.select('cart')
      .subscribe((items: Array<any>) => {
        this.cartItemsCount = _.reduce(_.map(items, 'quantity'), (sum: number, quantity: number) => sum + quantity) || 0;
        const totals = _.map(items, item => {
          return item.quantity * (item.product.hasSpecial ? item.product.priceSpecialAmount : item.product.price)
        });

        this.cartTotal = _.reduce(totals, (sum: number, total: number) => {
            return sum + total;
          }) || 0;
        const group = _.groupBy(items, 'product.currency');
        const currencies = _.keys(group);
        this.cartCurrency = currencies && currencies.length === 1 ? currencies[0] : null;
      });
  }
}
