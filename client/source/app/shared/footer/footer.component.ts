import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'fm-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  copyYear: number;

  constructor() { }

  ngOnInit() {
    this.copyYear = moment().year()
  }
}
