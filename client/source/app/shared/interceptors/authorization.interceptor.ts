import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TokenStoreService } from '../services/token-store.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {
  constructor(private tokenStore: TokenStoreService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.tokenStore.isExpired()) {
      return next.handle(req);
    }

    // Get the auth token from the service.
    const token = this.tokenStore.getToken();
    const headers = req.headers.set('Authorization', `${token.token_type} ${token.access_token}`);
    // Clone the request to add the new header.
    const authReq = req.clone({headers});
    // Pass on the cloned request instead of the original request.
    return next.handle(authReq);
  }
}
