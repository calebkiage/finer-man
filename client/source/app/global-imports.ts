/**
  * Author:          Caleb Kiage
  * Email:           caleb.kiage@gmail.com
  * Date:            2/15/17
  */

import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
