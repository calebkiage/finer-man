import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './shared/guards/auth-guard.service';
import { environment } from '../environments/environment';

const routes: Routes = [
  { path: '', loadChildren: 'app/home/home.module#HomeModule' },
  { path: 'account', loadChildren: 'app/account/account.module#AccountModule' },
  { path: 'orders', loadChildren: 'app/order/order.module#OrderModule', canActivate: [AuthGuardService] },
  { path: 'products', loadChildren: 'app/catalog/catalog.module#CatalogModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: !environment.production})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
