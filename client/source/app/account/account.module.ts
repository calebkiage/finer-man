import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountRoutingModule } from './account-routing.module';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    AccountRoutingModule
  ],
  declarations: [LoginComponent, AccountComponent, RegisterComponent]
})
export class AccountModule { }
