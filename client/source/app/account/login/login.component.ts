import { Component, OnInit } from '@angular/core';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { Logger } from '../../common/logger';
import { TokenStoreService } from '../../shared/services/token-store.service';
import { TokenService } from '../../shared/services/token.service';

@Component({
  selector: 'fm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  authorizationUri;
  error: string;
  loggedIn = false;
  loggingIn = false;
  message: string;
  private returnTo: string;

  constructor(private logger: Logger,
              private sanitizer: DomSanitizer,
              private route: ActivatedRoute,
              private router: Router,
              private tokenStore: TokenStoreService,
              titleService: Title) {
    titleService.setTitle('Finer Man - Login');
  }

  private afterLogin() {
    this.router.navigateByUrl(this.returnTo || '/').catch(e => {
      this.logger.error(e);
    });
  }

  logout() {
    this.tokenStore.clearToken();
    this.loggedIn = this.tokenStore.getToken() && !this.tokenStore.isExpired();
  }

  ngOnInit() {
    // Check whether we are logged in.
    this.loggedIn = this.tokenStore.getToken() && !this.tokenStore.isExpired();
    const hasEncode = (typeof encodeURIComponent === 'function');
    let redirectUri = environment.oauth.redirectUri;
    if (hasEncode) {
      redirectUri = encodeURIComponent(redirectUri);
    }
    const url = `${environment.oauth.authorizationCodeEndpointUri}?client_id=${environment.oauth.clientId}&response_type=token&redirect_uri=${redirectUri}`;
    this.authorizationUri = this.sanitizer.bypassSecurityTrustUrl(url);

    this.route.queryParams.subscribe(params=> {
      this.error = params['error'];
    });

    this.route.fragment.subscribe(fragment=> {
      if (!fragment || typeof fragment !== 'string') {
        return;
      }

      // Split fragment parts.
      let parts = fragment.split('&');
      let token: any = {};
      for (const part of parts) {
        let pair = part.split('=');
        if (pair.length > 1) {
          token[pair[0]] = pair[1];
        }
      }

      this.tokenStore.storeToken(token);

      this.loggingIn = false;
      this.afterLogin();
    });
  }
}
