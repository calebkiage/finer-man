import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';
import { match } from '../../common/custom-validators';
import { Gender } from '../../common/enums';
import { Logger } from '../../common/logger';

@Component({
  selector: 'fm-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  availableGenders: Array<{ key: any, value: string }>;
  registrationForm: FormGroup;
  validationMessages: any;

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private logger: Logger,
              private router: Router,
              titleService: Title) {
    titleService.setTitle('Finer Man - Register');
  }

  getField(fieldName, groupName) {
    let field = null;
    if (typeof fieldName === 'string') {
      if (typeof groupName === 'string') {
        field = this.registrationForm.controls[groupName]['controls'][fieldName];
      } else {
        field = this.registrationForm.controls[fieldName];
      }
    } else {
      field = this.registrationForm;
    }

    return field;
  }

  getError(fieldName, groupName) {
    if (typeof fieldName !== 'string' && typeof groupName !== 'string') {
      return;
    }

    let field = null;
    let validationMessage = null;

    if (groupName) {
      field = this.registrationForm.controls[groupName];
      validationMessage = this.validationMessages[groupName];

      if (fieldName) {
        field = field['controls'][fieldName];
        validationMessage = validationMessage[fieldName];
      }
    } else {
      field = this.registrationForm.controls[fieldName];
      validationMessage = this.validationMessages[fieldName];
    }

    return _.get(validationMessage, _.keys(field.errors));
  }

  hasErrors(fieldName = null, groupName = null) {
    const field = this.getField(fieldName, groupName);

    return field && field.errors && (field.dirty || field.touched);
  }

  submitRegistration() {
    if (this.registrationForm.invalid) {
      return;
    }

    this.http.post<{errors?}>(`${environment.backendUrl}/accounts/register`, this.registrationForm.value)
      .subscribe(response => {
        if (response.errors) {
          for (const key of _.keys(response.errors.field)) {
            this.registrationForm.controls[key].setErrors({'serverError': true});
            if (key.startsWith('password')) {
              this.validationMessages['passwords'][key].serverError = response.errors.field[key];
            } else {
              this.validationMessages[key].serverError = response.errors.field[key];
            }
          }

          this.logger.info(response);
        } else {
          this.router.navigateByUrl('/');
        }
      }, error => {
        this.logger.error(error);
      });
  }

  ngOnInit() {
    this.availableGenders = [];
    for (const gender in Gender) {
      if (typeof Gender[gender] === 'number') {
        this.availableGenders.push({key: Gender[gender], value: Gender[Gender[gender]]});
      }
    }

    this.registrationForm = this.fb.group({
      firstName: '',
      lastName: '',
      email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      gender: Gender.Other,
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordConfirmation: ['', Validators.required]
      }, {validator: match})
    });

    this.validationMessages = {
      firstName: {},
      lastName: {},
      email: {
        required: 'Please enter your email address.',
        pattern: 'Please enter a valid email. We will send your account activation instructions here.'
      },
      passwords: {
        match: 'The passwords don\'t match.',
        password: {
          required: 'Please enter your desired password.',
          minlength: 'The password must be at least 6 characters.'
        },
        passwordConfirmation: {
          required: 'Please type in your password again to help avoid spelling mistakes.'
        }
      }
    };
  }
}
