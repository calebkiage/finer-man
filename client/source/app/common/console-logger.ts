import { Logger } from './logger';

/**
 * Author:          Caleb Kiage
 * Organization:    Kiage Systems
 * Email:           caleb.kiage@gmail.com
 * Date:            17/02/17
 */
export class ConsoleLogger extends Logger {
  log(level: string, message) {
    switch (level.toLocaleLowerCase()) {
      case 'warn':
        console.warn(message);
        break;
      case 'error':
        console.error(message);
        break;
      case 'info':
      default:
        console.log(message);
        break;
    }
  }
}
