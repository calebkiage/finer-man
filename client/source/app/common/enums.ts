/**
  * Author:          Caleb Kiage
  * Organization:    Kiage Systems
  * Email:           caleb.kiage@gmail.com
  * Date:            19/02/17
  */

export enum Gender {
  Other, Male, Female
}
