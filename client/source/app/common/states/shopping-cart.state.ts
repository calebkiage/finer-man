import { Action } from '@ngrx/store';
import * as _ from 'lodash';

/**
  * Author:          Caleb Kiage
  * Organization:    Kiage Systems
  * Email:           caleb.kiage@gmail.com
  * Date:            20/02/17
  */
export const CART_ADD = 'cart.add-item';
export const CART_REMOVE = 'cart.remove-item';
export const CART_RESET = 'cart.reset';

export interface CartState {
  items: Array<{quantity: number, product: {id: number}}>;
}

export function cartReducer (state: Array<{quantity: number, product: {id: number}}> = [], action) {
  // Create a new array. Reducers should be pure functions.
  const newState = state.slice();
  let existing = null;

  switch (action.type) {
    case CART_ADD:
      existing = _.find(newState, (item)=> {
        return item.product && item.product.id === action.payload.id
      });

      if (existing) {
        existing.quantity++;
      } else {
        newState.push({quantity: 1, product: action.payload});
      }
      return newState;
    case CART_REMOVE:
      const index = _.findIndex(newState, (item)=> {
        return item.product && item.product.id === action.payload.id
      });
      existing = index >= 0 ? newState[index] : null;

      if (existing.quantity > 1) {
        existing.quantity--;
      } else {
        newState.splice(index, 1);
      }
      return newState;
    case CART_RESET:
      return [];
    default:
      return newState;
  }
}
