/**
  * Author:          Caleb Kiage
  * Organization:    Kiage Systems
  * Email:           caleb.kiage@gmail.com
  * Date:            17/02/17
  */

export abstract class Logger {
  info(message) {
    this.log('info', message);
  }

  error(message) {
    this.log('error', message);
  }

  warn(message) {
    this.log('warn', message);
  }

  abstract log(level: any, message: string);
}
