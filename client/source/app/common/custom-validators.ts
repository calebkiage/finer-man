import { AbstractControl, FormGroup } from '@angular/forms';
import * as _ from 'lodash';

/**
  * Author:          Caleb Kiage
  * Organization:    Kiage Systems
  * Email:           caleb.kiage@gmail.com
  * Date:            20/02/17
  */

export function match(group: FormGroup) {
  if (_.reduce(group.controls, (c1: AbstractControl, c2: AbstractControl)=> c1.value === c2.value ? c1 : NaN)) {
    return null;
  }

  return {
    match: true
  }
}
