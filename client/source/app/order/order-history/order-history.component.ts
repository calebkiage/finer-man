import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../shared/services/orders.service';

@Component({
  selector: 'fm-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
  orders;

  constructor(private ordersService: OrdersService) { }

  ngOnInit() {
    this.ordersService.getOrders().subscribe(ordersPage => {
      this.orders = ordersPage.content;
    });
  }
}
