import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import groupBy from 'lodash/groupBy';
import keys from 'lodash/keys';
import map from 'lodash/map';
import reduce from 'lodash/reduce';
import { CART_REMOVE, CART_RESET } from '../../common/states/shopping-cart.state';
import { OrdersService } from '../../shared/services/orders.service';

@Component({
  selector: 'fm-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  cartCurrency;
  cartItems: Array<any>;
  cartTotal;
  cartItemsCount: number;
  checkoutModel: any = {};
  orderId;

  constructor(private store: Store<any>, private ordersService: OrdersService) {
  }

  placeOrder() {
    const payload = this.checkoutModel;
    payload.orderDetails = this.cartItems;
    this.ordersService.createOrder(payload).subscribe(orderId => {
      this.orderId = orderId;

      // Clear cart.
      this.store.dispatch({type: CART_RESET});
    });
  }

  removeItem(product) {
    this.store.dispatch({type: CART_REMOVE, payload: product});
  }

  ngOnInit() {
    this.store.select('cart').map((items: any[]) => {
      return map(items, (item) => {
        item.total = item.quantity * (item.product.hasSpecial ? item.product.priceSpecialAmount : item.product.price);
        return item;
      });
    }).subscribe(items => {
      this.cartItems = items;
      this.cartItemsCount = reduce(map(this.cartItems, 'quantity'), (sum: number, quantity: number) => sum + quantity) || 0;
      this.cartTotal = reduce(map(this.cartItems, item => {
        return item.total;
      }), (sum, total) => {
        return sum + total;
      }) || 0;

      this.cartCurrency = this.cartItems.map(it => {
        const group = groupBy(it, 'product.currency');
        const currencies = keys(group);
        return currencies && currencies.length === 1 ? currencies[0] : null;
      });
    });
  }
}
