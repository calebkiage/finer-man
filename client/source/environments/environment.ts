// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const appUrl = 'http://localhost:4200';
const serverUrl = 'http://localhost:8100';

export const environment = {
  backendUrl: serverUrl,
  oauth: {
    authorizationCodeEndpointUri: `${serverUrl}/oauth/authorize`,
    checkTokenEndpointUri: `${serverUrl}/oauth/check_token`,
    clientId: 'finer-man-app',
    clientSecret: 'secret',
    redirectUri: `${appUrl}/account/login`,
    tokenEndpointUri: `${serverUrl}/oauth/token`,
    userEndpointUri: `${serverUrl}/user`
  },
  production: false
};
