const appUrl = 'http://finer-man-client.s3-website.eu-west-2.amazonaws.com';
const serverUrl = 'http://finer-man-api.eu-west-2.elasticbeanstalk.com';

export const environment = {
  backendUrl: serverUrl,
  oauth: {
    authorizationCodeEndpointUri: `${serverUrl}/oauth/authorize`,
    checkTokenEndpointUri: `${serverUrl}/oauth/check_token`,
    clientId: 'finer-man-app',
    clientSecret: 'secret',
    redirectUri: `${appUrl}/account/login`,
    tokenEndpointUri: `${serverUrl}/oauth/token`,
    userEndpointUri: `${serverUrl}/user`
  },
  production: false
};
